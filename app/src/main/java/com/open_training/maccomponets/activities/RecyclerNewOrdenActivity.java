package com.open_training.maccomponets.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;
import android.view.View.OnClickListener;

import com.open_training.maccomponets.R;
import com.open_training.maccomponets.objects.ejercicio2.FoodType;
import com.open_training.maccomponets.objects.ejercicio2.MacOrden;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Mac;

public class RecyclerNewOrdenActivity extends AppCompatActivity {

    private EditText etProduct;
    private EditText etCon;
    private EditText etSin;
    private Spinner sOptions;
    private ImageView ivSelected;
    public Integer correlativo;
    private MacOrden macTemp2;
    private ArrayList<String> CArrayCon;
    private ArrayList<String> CArraySin;
    public RadioButton cIngre11;
    public RadioButton cIngre22;
    private RadioButton cIngre33;
    private RadioButton sIngre11;
    private RadioButton sIngre22;
    private RadioButton sIngre33;
    private RadioGroup cRadio1;
    private RadioGroup cRadio2;
    private RadioGroup cRadio3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_new_orden);


        if ((savedInstanceState != null)
                &&(savedInstanceState.containsKey("selectedImg"))){
            savedInstanceState.getInt("selectedImg");
        }



        initComponents();
    }


    private void initComponents (){
        ivSelected = (ImageView) findViewById(R.id.iv_imageOrden);
        sOptions = (Spinner) findViewById(R.id.s_newOrden);
        CArrayCon= new ArrayList<>();
        CArraySin= new ArrayList<>();

         cRadio1=(RadioGroup)findViewById(R.id.RadioG1);
         cRadio2=(RadioGroup)findViewById(R.id.RadioG2);
         cRadio3=(RadioGroup)findViewById(R.id.RadioG3);

         cIngre11 = (RadioButton)cRadio1.findViewById(R.id.radio1Con);
         sIngre11 = (RadioButton)cRadio1.findViewById(R.id.radio1Sin);
         cIngre22 = (RadioButton)cRadio2.findViewById(R.id.radio2Con);
         sIngre22 = (RadioButton)cRadio2.findViewById(R.id.radio2Sin);
         cIngre33 = (RadioButton)cRadio3.findViewById(R.id.radio3Con);
         sIngre33 = (RadioButton)cRadio3.findViewById(R.id.radio3Sin);



        String[] options =
                getResources().getStringArray(R.array.order_opt);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,android.R.layout.simple_spinner_dropdown_item,options
        );

        sOptions.setAdapter(adapter);

        sOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                cIngre11.setChecked(true);
                cIngre22.setChecked(true);
                cIngre33.setChecked(true);
                switch (position) {
                    case 0:

                        ivSelected.setImageResource(
                                R.drawable.cafe);
                        cIngre11.setText("Leche");
                        sIngre11.setText("Leche");

                        cIngre22.setText("Azucar");
                        sIngre22.setText("Azucar");

                        cIngre33.setText("Decafeinado");
                        sIngre33.setText("Decafeinado");
                        correlativo=0;
                        break;
                    case 1:
                        ivSelected.setImageResource(
                                R.drawable.mcflurry);
                        cIngre11.setText("Oreo");
                        sIngre11.setText("Oreo");

                        cIngre22.setText("Chocolate");
                        sIngre22.setText("Chocolate");

                        cIngre33.setText("Con Caramelo");
                        sIngre33.setText("Sin Caramelo");
                        correlativo=1;
                        break;
                    case 2:
                        ivSelected.setImageResource(
                                R.drawable.hamburguesa);
                        cIngre11.setText("Tomate");
                        sIngre11.setText("Tomate");

                        cIngre22.setText("Lechuca");
                        sIngre22.setText("Lechuca");

                        cIngre33.setText("Carne");
                        sIngre33.setText("Carne");
                        correlativo=2;
                        break;
                }
            }

            @Override
            public void    onNothingSelected( AdapterView<?> adapterView){
            }
        });


    }

    public void addNewOrden (View view){


        if (cIngre11.isChecked()) {
            CArrayCon.add(cIngre11.getText().toString());
        }else{
            CArraySin.add(sIngre11.getText().toString());
        }

        if (cIngre22.isChecked()) {
            CArrayCon.add(cIngre22.getText().toString());
        }else{
            CArraySin.add(sIngre22.getText().toString());
        }

        if (cIngre33.isChecked()) {
            CArrayCon.add(cIngre33.getText().toString());
        }else{
            CArraySin.add(sIngre33.getText().toString());
        }



       // if ((newCon!= null) && (!newCon.trim().isEmpty())){
            switch(correlativo){
                case 0:
                    macTemp2 = new MacOrden();
                    macTemp2.setFoodType(FoodType.CAFE);
                    macTemp2.setConList(( CArrayCon));
                    macTemp2.setSinList(( CArraySin));
                    break;
                case 1:

                    macTemp2 = new MacOrden();
                    macTemp2.setFoodType(FoodType.MAC_FLURRY);
                    macTemp2.setConList(( CArrayCon));
                    macTemp2.setSinList(( CArraySin));
                    break;
                case 2:
                    macTemp2 = new MacOrden();
                    macTemp2.setFoodType(FoodType.HAMBURGUESA);
                    macTemp2.setConList(( CArrayCon));
                    macTemp2.setSinList(( CArraySin));
                    break;
          }


            Intent intent = new Intent();
            intent.putExtra(RecyclerActivity.RESULT_ST,macTemp2);
            setResult(RESULT_OK,intent);
            finish();


        //}else{
       //     Toast.makeText(this,"Debes ingresar la nueva Orden.",Toast.LENGTH_SHORT).show();
       // }
    }
}
