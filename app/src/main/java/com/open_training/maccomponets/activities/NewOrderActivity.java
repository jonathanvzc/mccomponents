package com.open_training.maccomponets.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.open_training.maccomponets.R;

public class NewOrderActivity extends AppCompatActivity {

    private EditText etNewOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);
        etNewOrder = findViewById(R.id.et_new_order);
    }
    public  void    addElement(View view){

        String newOrder = etNewOrder.getText().toString();
        if ((newOrder!= null) && (!newOrder.trim().isEmpty())){

            Intent intent = new Intent();
            intent.putExtra(ResultActivity.RESULT_STR,newOrder);

            setResult(RESULT_OK,intent);

            finish();


        }else{
            Toast.makeText(this,"Debes ingresar la nueva Orden.",Toast.LENGTH_SHORT).show();
        }
    }
}
