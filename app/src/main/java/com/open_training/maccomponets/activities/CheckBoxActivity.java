package com.open_training.maccomponets.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.open_training.maccomponets.R;

public class CheckBoxActivity extends AppCompatActivity {
    private CheckBox cbxAtm;
    private CheckBox cbxBag;
    private CheckBox cbxBasquet;
    private CheckBox cbxBox;
    private CheckBox cbxBriefcase;
    private CheckBox cbxCalculator;

    private ImageView ivAt;
    private ImageView ivBag;
    private ImageView ivBasquet;
    private ImageView ivBox;
    private ImageView ivBriefcase;
    private ImageView ivCalculator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_box);
        InitComponents();
    }

    private  void InitComponents (){
        cbxAtm = (CheckBox) findViewById(R.id.cbx_opt_atm);
        cbxAtm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    ivAt.setVisibility(View.VISIBLE);

                }else{
                    ivAt.setVisibility(View.GONE);
                }
            }
        });

        cbxBag = (CheckBox) findViewById(R.id.cbx_opt_bag);
        cbxBag.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    ivBag.setVisibility(View.VISIBLE);

                }else{
                    ivBag.setVisibility(View.GONE);
                }
            }
        });


        cbxBasquet = (CheckBox) findViewById(R.id.cbx_opt_basquet);
        cbxBasquet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    ivBasquet.setVisibility(View.VISIBLE);

                }else{
                    ivBasquet.setVisibility(View.GONE);
                }
            }
        });

        cbxBox = (CheckBox) findViewById(R.id.cbx_opt_box);
        cbxBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    ivBox.setVisibility(View.VISIBLE);

                }else{
                    ivBox.setVisibility(View.GONE);
                }
            }
        });


        cbxBriefcase = (CheckBox) findViewById(R.id.cbx_opt_brief);
        cbxBriefcase.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    ivBriefcase.setVisibility(View.VISIBLE);

                }else{
                    ivBriefcase.setVisibility(View.GONE);
                }
            }
        });

        cbxCalculator = (CheckBox) findViewById(R.id.cbx_opt_cal);
        cbxCalculator.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    ivCalculator.setVisibility(View.VISIBLE);

                }else{
                    ivCalculator.setVisibility(View.GONE);
                }
            }
        });



        ivAt = (ImageView) findViewById(R.id.iv_atm);
        ivBag = (ImageView) findViewById(R.id.iv_bag);
        ivBasquet = (ImageView) findViewById(R.id.iv_basquet);
        ivBox = (ImageView) findViewById(R.id.iv_box);
        ivBriefcase = (ImageView) findViewById(R.id.iv_Briefcase);
        ivCalculator = (ImageView) findViewById(R.id.iv_calculator);

    }

}
