package com.open_training.maccomponets.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import com.open_training.maccomponets.R;
import com.open_training.maccomponets.objects.adapters.ItemRecyclerAdapter;
import com.open_training.maccomponets.objects.ejercicio2.FoodType;
import com.open_training.maccomponets.objects.ejercicio2.MacOrden;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RecyclerActivity extends AppCompatActivity {
    public  final  int NEW_ORDER_CODE =321;
    public   static final String RESULT_ST = "strRESULT";
    public   static final String RESULT_ST2 = "strRESULT";
    private RecyclerView rvListOrder;
    private ItemRecyclerAdapter itemRecyclerAdapter;
    List<MacOrden> result = new ArrayList<>();

    // nuevo
    private ArrayList<String> Lvnew;
    private ListView lvOpt;
    private ArrayAdapter<String> adapter;
    private ListView rvrecup;
    private ArrayAdapter<String> orderAdapter;
    private ItemRecyclerAdapter orderAdapter2;
    List<MacOrden> resultBackup2 = new ArrayList<>();
    //private ArrayList<MacOrden> resultBackup2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        if (savedInstanceState != null) {
            result = savedInstanceState.getParcelableArrayList(RESULT_ST2);
        }
        setContentView(R.layout.activity_recycler);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // startActivityForResult(new Intent(RecyclerActivity.this, RecyclerNewOrdenActivity.class), NEW_ORDER_CODE);
                startActivityForResult(new Intent(RecyclerActivity.this, AddMacOrderActivity.class), NEW_ORDER_CODE);
              /*  Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });
        initComponents();
    }


    private  void    initComponents(){
        rvListOrder = findViewById(R.id.rv_list_order);
        GridLayoutManager gridLayoutManager = new GridLayoutManager( this,1) ;
        rvListOrder.setLayoutManager(gridLayoutManager);

        itemRecyclerAdapter = new ItemRecyclerAdapter(result,this , R.layout.item_line_order);
       // itemRecyclerAdapter = new ItemRecyclerAdapter(getOrders(),this , R.layout.item_line_order);
        rvListOrder.setAdapter(itemRecyclerAdapter);
    }

    private List<MacOrden> getOrders(){

        MacOrden macTemp1 = new MacOrden();
        macTemp1.setFoodType(FoodType.CAFE);
        macTemp1.setConList(Arrays.asList( "Leche","Azucar"));
        macTemp1.setSinList(Arrays.asList( "Decafeinado"));
        result.add(macTemp1);

        macTemp1 = new MacOrden();
        macTemp1.setFoodType(FoodType.HAMBURGUESA);
        macTemp1.setConList(Arrays.asList( "Pan","Carne","Tomate","Lechuga"));
        macTemp1.setSinList(Arrays.asList( "Cebolla"));
        result.add(macTemp1);

        macTemp1 = new MacOrden();
        macTemp1.setFoodType(FoodType.MAC_FLURRY);
        macTemp1.setConList(Arrays.asList( "Oreo","Chocolate"));
        macTemp1.setSinList(Arrays.asList( "Caramelo"));
        result.add(macTemp1);

        macTemp1 = new MacOrden();
        macTemp1.setFoodType(FoodType.CAFE);
        macTemp1.setConList(Arrays.asList( "Leche","Azucar"));
        macTemp1.setSinList(Arrays.asList( "Decafeinado"));
        result.add(macTemp1);

        return  result;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if((requestCode == NEW_ORDER_CODE)&& (resultCode == RESULT_OK)&& (data != null)){
            MacOrden macTemp2 = (MacOrden) data.getParcelableExtra(RESULT_ST);

            result.add(macTemp2);
            itemRecyclerAdapter.notifyDataSetChanged();
        }
    }

      @Override
    protected  void onSaveInstanceState(Bundle state){
        super.onSaveInstanceState(state);

          if (state != null) {

              resultBackup2= result;
              state.putParcelableArrayList(RESULT_ST2, (ArrayList<? extends Parcelable>) resultBackup2);
          }

          Log.i("RecyclerActivity","onSaveInstanceState");
        super.onSaveInstanceState(state);
    }


    @Override
    protected  void onRestoreInstanceState(Bundle saveInstanceState){
        super.onRestoreInstanceState(saveInstanceState);
        Log.i("RecyclerActivity","onRestoreInstanceState");
        }
    }

