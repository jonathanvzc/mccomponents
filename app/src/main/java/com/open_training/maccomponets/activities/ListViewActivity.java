package com.open_training.maccomponets.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import com.open_training.maccomponets.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {

    private EditText etElemnet;
    private ListView lvOpt;
    private ImageView ivSelected;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> Lvnew;
    private List <String>  Lvnew2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        initComponents();
    }

    private  void   initComponents(){
        Lvnew= new ArrayList<>();
        etElemnet =(EditText) findViewById(R.id.et_new_element) ;
        etElemnet.getText().toString();
        //Llenar la lista como se llena el Spiner

        lvOpt = (ListView) findViewById(R.id.lv_opt);

        String[] options =
                getResources().getStringArray(R.array.img_opt);


          for (String e : options )
           {
                System.out.println(e);
                Lvnew.add(e);
           }

        adapter = new ArrayAdapter<String>(
                this,android.R.layout.simple_list_item_1,options
        );

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,android.R.layout.simple_list_item_1,options
        );

        lvOpt.setAdapter(adapter);

    }


    public void agregar  (View view) {
        String elemento =etElemnet.getText().toString();
        if (( elemento != null) &&( !elemento.trim().isEmpty())); {
            Lvnew.add(etElemnet.getText().toString());
            ArrayAdapter<String> arrayAdapter  = new ArrayAdapter<String>(
                    this,android.R.layout.simple_list_item_1,Lvnew);

            lvOpt.setAdapter(arrayAdapter );
            etElemnet.setText("");
        }
    }
}
