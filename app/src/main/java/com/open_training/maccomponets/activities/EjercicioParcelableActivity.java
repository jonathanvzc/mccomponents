package com.open_training.maccomponets.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.open_training.maccomponets.R;
import com.open_training.maccomponets.objects.ejercicio1.Cafe;
import com.open_training.maccomponets.objects.ejercicio1.Hamburguesa;
import com.open_training.maccomponets.objects.ejercicio1.Macflurry;
import com.open_training.maccomponets.objects.NuevaOrden;

import java.util.List;

public class EjercicioParcelableActivity extends AppCompatActivity {

    private TextView tvNuevaOrden;
    private NuevaOrden nuevaOrden;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio_parcelable);
        initComponets();
    }

        private void initComponets(){
            NuevaOrden nuevaOrden = getIntent().getParcelableExtra( OptionsActivity.NUEVA_ORDEN);
            tvNuevaOrden =(TextView) findViewById(R.id.tv_nueva_orden);

            List<Hamburguesa> hamburguesaList =nuevaOrden.getHamburguesaList();
            List<Macflurry> macflurryList  =nuevaOrden.getMacflurryList();
            List<Cafe> cafeList = nuevaOrden.getCafeList();

           String misPedidos="Hamburguesa";
            for (Hamburguesa hamburguesa:hamburguesaList) {
                misPedidos += "\n Pepinillos: " + getVal(hamburguesa.isPepinillos());
                misPedidos += "\n Tomates: " + getVal(hamburguesa.isTomate());
                misPedidos += "\n Lechuga: " + getVal(hamburguesa.isLechuga());
                misPedidos += "\n Cebolla: " + getVal(hamburguesa.isCebolla());
                misPedidos += "\n\n";
            }


            misPedidos += "macflurry ";
            for(Macflurry macflurry:macflurryList){
                misPedidos += "\n Toping: "+ getVal(macflurry.isTopin());
                misPedidos += "\n Oreo: "+ getVal(macflurry.isOreo());
                misPedidos += "\n M&M: " + getVal(macflurry.isMym());
                misPedidos += "\n\n";
            }


            misPedidos += "Cafecitos ";
            for(Cafe cafe:cafeList){
                misPedidos += "\n Azucar: "+ getVal(cafe.isAzucar());
                misPedidos += "\n Cafeina: "+ getVal(cafe.isCafeina());
                misPedidos += "\n Leche: " + getVal(cafe.isLeche());
                misPedidos += "\n\n";
            }

            tvNuevaOrden.setText(misPedidos);

    }

    private String getVal(boolean bool){
        return((bool)? "Si": "No");
    }
}



       // System.out.println(" uioioiuoiuoi "+ nuevaorden.getHamburguesaList().iterator().next().isCebolla());
    //  System.out.println(" su macflurry es "+ nuevaOrden.get ());
    //  System.out.println(" su cafe es "+ nuevaOrden.getCafeList());




