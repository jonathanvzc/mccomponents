package com.open_training.maccomponets.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.open_training.maccomponets.R;
import com.open_training.maccomponets.objects.ejercicio2.MacOption;
import com.open_training.maccomponets.objects.adapters.MacOrderAdapter;
import com.open_training.maccomponets.objects.ejercicio2.FoodType;
import com.open_training.maccomponets.objects.ejercicio2.MacOrden;

import java.util.ArrayList;
import java.util.List;

public class AddMacOrderActivity extends AppCompatActivity {
    private ImageView ivSelected;
    private Spinner sMacOrder;
    private RecyclerView rvOptions;
    private MacOrderAdapter macOrderAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mac_order);

        initComponents();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.add_mac_order_menu,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add_mac_order:
                MacOrden macOrden  = new MacOrden();
                macOrden.setFoodType(FoodType.getFoodType(sMacOrder.getSelectedItemPosition()));
                for(MacOption macOption:macOrderAdapter.getMacOptions()){
                    if(macOption.isSelected()){
                        macOrden.getConList().add(macOption.getStrOption());
                    }else{
                        macOrden.getSinList().add(macOption.getStrOption());
                    }
                }
                Intent resultIntent = new Intent();
                resultIntent.putExtra(RecyclerActivity.RESULT_ST,macOrden);
                setResult(RESULT_OK,resultIntent);
                break;
        }

        finish();
        return super.onOptionsItemSelected(item);

    }

    private void initComponents(){
        ivSelected = findViewById(R.id.iv_selected);
        sMacOrder = findViewById(R.id.s_mac_order);
        rvOptions = findViewById(R.id.rv_order_options);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,3);
        rvOptions.setLayoutManager(gridLayoutManager);

        sMacOrder.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.order_opt)));

        sMacOrder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                changeOption();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        changeOption();

    }

    private void changeOption(){
        switch (sMacOrder.getSelectedItemPosition()){
            case 0:
                ivSelected.setImageResource(R.drawable.cafe);
                macOrderAdapter = new MacOrderAdapter(
                        generateMacOpt(getResources().getStringArray(R.array.Cafe_option)),this);
                break;
            case 1:
                ivSelected.setImageResource(R.drawable.mcflurry);
                macOrderAdapter = new MacOrderAdapter(
                        generateMacOpt(getResources().getStringArray(R.array.macflurry_option)),this);

                break;
            case 2:
                ivSelected.setImageResource(R.drawable.hamburguesa);
                macOrderAdapter = new MacOrderAdapter(
                        generateMacOpt(getResources().getStringArray(R.array.hamburgesa_option)),this);
                break;
        }

        rvOptions.setAdapter(macOrderAdapter);
    }

    private List<MacOption> generateMacOpt(String[] macOpt){
        List<MacOption> result = new ArrayList<>();

        for (String strOpt : macOpt){
            result.add(new MacOption(strOpt,false));
        }

        return  result;
    }

}
