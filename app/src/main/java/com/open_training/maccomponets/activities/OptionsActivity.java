package com.open_training.maccomponets.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.open_training.maccomponets.R;
import com.open_training.maccomponets.SplashScreenActivity;
import com.open_training.maccomponets.objects.ejercicio1.Cafe;
import com.open_training.maccomponets.objects.ejercicio1.Hamburguesa;
import com.open_training.maccomponets.objects.ejercicio1.Macflurry;
import com.open_training.maccomponets.objects.NuevaOrden;
import com.open_training.maccomponets.objects.Orden;

import java.util.ArrayList;
import java.util.List;

public class OptionsActivity extends AppCompatActivity {
    public   static  final  String NUEVA_ORDEN  ="llave";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        Orden orden = getIntent().getParcelableExtra(SplashScreenActivity.key);



        Toast.makeText(this,"se pidio "+ orden.getCanCafe()+" Cafes "
                        + orden.getCanHamburguesa() +" Hamburguesas "
                        + orden.getCanMacFlurry() +" Mcflurry "
                        + orden.getCanPapas() +" Papas "
                        ,Toast.LENGTH_LONG).show();
    }

    public void ejercicioParcelable(View view){
        List<Hamburguesa> HamburguesaList = new ArrayList<>();
        List<Macflurry> MacflurryList   =new ArrayList<>();
       // List<Macflurry> MacflurryList = new ArrayList<>();
        List<Cafe> CafeList = new ArrayList<>();



        Hamburguesa hamburguesa1 = new Hamburguesa(false,true,true,true);
        Hamburguesa hamburguesa2 = new Hamburguesa(true,false, false, false);
        HamburguesaList.add(hamburguesa1);
        HamburguesaList.add(hamburguesa2);

        Macflurry mcFlury1 = new Macflurry(false, true,false);
        Macflurry mcFlury2 = new Macflurry(false, false,true);
        MacflurryList.add(mcFlury1);
        MacflurryList.add(mcFlury2);

        Cafe cafe1 = new Cafe(true, false, true);
        Cafe cafe2 = new Cafe(false, true, true);
        CafeList.add(cafe1);
        CafeList.add(cafe2);




        Intent intent = new Intent(this, EjercicioParcelableActivity.class);


       /* CafeList.add(new Cafe(true,true,true));
        HamburguesaList.add(new Hamburguesa(true,true, true,true   ));
        MacflurryList.add(new Macflurry(true,false ,true));*/


        NuevaOrden nuevaOrden = new NuevaOrden(HamburguesaList,MacflurryList,CafeList);
        intent.putExtra( NUEVA_ORDEN, nuevaOrden );
        startActivity(intent);


    }

    public void goToCicle(View view){
        startActivity(new Intent(this,CicleActivity.class  ));
    }

    @Override
    public Intent getIntent() {
        return super.getIntent();
    }

    public void goToCbx(View view){
        startActivity(new Intent(this,CheckBoxActivity.class));
    }

    public void goTOListView (View view){
        startActivity(new Intent(this,ListViewActivity.class));
    }
    
    public  void goToResult (View view) { startActivity( new Intent(this, ResultActivity.class));}

    public  void goToRecycler (View view) { startActivity( new Intent(this,RecyclerActivity.class));}
}
