package com.open_training.maccomponets;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.open_training.maccomponets.activities.OptionsActivity;
import com.open_training.maccomponets.objects.Orden;

public class SplashScreenActivity extends AppCompatActivity {
    private Handler mHandler;
    public   static  final  String key  ="llave";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mHandler = new  Handler();
        mHandler.postDelayed(runOptions,5_000);
    }
    private Runnable runOptions = new Runnable() {
        @Override
        public void run() {
            goToOption ();
        }
    };

    //private void  goToOption () {
     //   startActivity( new Intent(this, OptionsActivity.class));
  //  }
    private void  goToOption () {
        Intent intent = new Intent(this, OptionsActivity.class);
        Orden orden = new Orden (3,0,3,0);
        intent.putExtra( key, orden );

        startActivity(intent);
    }

}
