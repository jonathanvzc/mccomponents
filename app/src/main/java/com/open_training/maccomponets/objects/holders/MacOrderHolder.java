package com.open_training.maccomponets.objects.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;

import com.open_training.maccomponets.R;
import com.open_training.maccomponets.objects.ejercicio2.MacOption;


/**
 * Created by jonathan.zepeda on 23/11/2017.
 */

public class MacOrderHolder extends RecyclerView.ViewHolder {

    private CheckBox cbxOption;

    public MacOrderHolder(View itemView) {
        super(itemView);

        cbxOption= itemView.findViewById(R.id.cbx_option);
    }

    public CheckBox getCbxOption() {
        return cbxOption;
    }

    public void setCbxOption(CheckBox cbxOption) {
        this.cbxOption = cbxOption;
    }

    public  void    setMacOption (MacOption macOption){
        cbxOption.setText(macOption.getStrOption());
    }

}
