package com.open_training.maccomponets.objects.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.open_training.maccomponets.R;
import com.open_training.maccomponets.objects.ejercicio2.MacOption;
import com.open_training.maccomponets.objects.holders.MacOrderHolder;

import java.util.List;

/**
 * Created by jonathan.zepeda on 23/11/2017.
 */

public class MacOrderAdapter extends RecyclerView.Adapter<MacOrderHolder> {


    private List<MacOption> macOptions;
    private Activity activity;

    public MacOrderAdapter(List<MacOption> macOptions , Activity activity)
    {
        this.macOptions= macOptions;
        this.activity = activity;
    }


    @Override
    public MacOrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new MacOrderHolder(LayoutInflater.from(activity).inflate(
                R.layout.item_new_order,parent,false));
    }

    @Override
    public void onBindViewHolder(MacOrderHolder holder, int position) {
        final   MacOption macOption = macOptions.get(position);
        holder.setMacOption(macOption);

       holder.getCbxOption().setOnCheckedChangeListener(
               new CompoundButton.OnCheckedChangeListener() {
                   @Override
                   public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                       macOption.setSelected(b);
                   }
               }
       );

    }




    public  List<MacOption> getMacOptions ()
    {
        return macOptions;
    }

    public void setMacOptions(List<MacOption> macOptions)
    {
        this.macOptions = macOptions;
    }


    @Override
    public int getItemCount()
    {
      return   macOptions.size();
    }

}
