package com.open_training.maccomponets.objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Constructor;

/**
 * Created by jonathan.zepeda on 26/09/2017.
 */

public class Orden implements Parcelable {

    private int canHamburguesa;
    private  int canMacFlurry;
    private  int canPapas;
    private  int canCafe;

    public Orden(int canHamburguesa, int canMacFlurry, int canPapas, int canCafe) {
        this.canHamburguesa = canHamburguesa;
        this.canMacFlurry = canMacFlurry;
        this.canPapas = canPapas;
        this.canCafe = canCafe;
    }

    protected Orden(Parcel in) {
        canHamburguesa = in.readInt();
        canMacFlurry = in.readInt();
        canPapas = in.readInt();
        canCafe = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(canHamburguesa);
        dest.writeInt(canMacFlurry);
        dest.writeInt(canPapas);
        dest.writeInt(canCafe);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Orden> CREATOR = new Parcelable.Creator<Orden>() {
        @Override
        public Orden createFromParcel(Parcel in) {
            return new Orden(in);
        }

        @Override
        public Orden[] newArray(int size) {
            return new Orden[size];
        }
    };

    public int getCanHamburguesa() {
        return canHamburguesa;
    }

    public void setCanHamburguesa(int canHamburguesa) {
        this.canHamburguesa = canHamburguesa;
    }

    public int getCanMacFlurry() {
        return canMacFlurry;
    }

    public void setCanMacFlurry(int canMacFlurry) {
        this.canMacFlurry = canMacFlurry;
    }

    public int getCanPapas() {
        return canPapas;
    }

    public void setCanPapas(int canPapas) {
        this.canPapas = canPapas;
    }

    public int getCanCafe() {
        return canCafe;
    }

    public void setCanCafe(int canCafe) {
        this.canCafe = canCafe;
    }
}