package com.open_training.maccomponets.objects.ejercicio1;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jonathan.zepeda on 17/10/2017.
 */

public class Hamburguesa implements Parcelable {

    private boolean pepinillos;
    private boolean Cebolla;
    private boolean Lechuga;
    private boolean tomate;

    public Hamburguesa() {
    }

    public Hamburguesa(boolean pepinillos, boolean cebolla, boolean lechuga, boolean tomate) {
        this.pepinillos = pepinillos;
        Cebolla = cebolla;
        Lechuga = lechuga;
        this.tomate = tomate;
    }

    protected Hamburguesa(Parcel in) {
        pepinillos = in.readByte() != 0x00;
        Cebolla = in.readByte() != 0x00;
        Lechuga = in.readByte() != 0x00;
        tomate = in.readByte() != 0x00;
    }

    public boolean isPepinillos() {
        return pepinillos;
    }

    public void setPepinillos(boolean pepinillos) {
        this.pepinillos = pepinillos;
    }

    public boolean isCebolla() {
        return Cebolla;
    }

    public void setCebolla(boolean cebolla) {
        Cebolla = cebolla;
    }

    public boolean isLechuga() {
        return Lechuga;
    }

    public void setLechuga(boolean lechuga) {
        Lechuga = lechuga;
    }

    public boolean isTomate() {
        return tomate;
    }

    public void setTomate(boolean tomate) {
        this.tomate = tomate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (pepinillos ? 0x01 : 0x00));
        dest.writeByte((byte) (Cebolla ? 0x01 : 0x00));
        dest.writeByte((byte) (Lechuga ? 0x01 : 0x00));
        dest.writeByte((byte) (tomate ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Hamburguesa> CREATOR = new Parcelable.Creator<Hamburguesa>() {
        @Override
        public Hamburguesa createFromParcel(Parcel in) {
            return new Hamburguesa(in);
        }

        @Override
        public Hamburguesa[] newArray(int size) {
            return new Hamburguesa[size];
        }
    };
}