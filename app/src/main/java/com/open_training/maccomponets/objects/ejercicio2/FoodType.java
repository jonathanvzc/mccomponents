package com.open_training.maccomponets.objects.ejercicio2;

import android.content.Context;

import com.open_training.maccomponets.R;

import java.net.ContentHandler;

/**
 * Created by jonathan.zepeda on 02/11/2017.
 */

public enum FoodType {
    CAFE,
    MAC_FLURRY,
    HAMBURGUESA;


    public String toString(Context context){
            switch (this){
                case CAFE:
                    return context.getString(R.string.opt_cafe);
                case MAC_FLURRY:
                    return context.getString(R.string.opt_macflurry);
                case HAMBURGUESA:
                    return context.getString(R.string.opt_hamburger);
                    default:
                        return "";
            }
    }


    public int getImage(){
        switch (this   ){
            case CAFE:
                return R.drawable.cafe;
            case MAC_FLURRY:
                return R.drawable.mcflurry;
            case HAMBURGUESA:
                return  R.drawable.hamburguesa;
            default:
                return R.drawable.box;
        }
    }

    public  static FoodType getFoodType (int position ){
        switch (position){
            case    0:
                return  CAFE;
            case   1:
                return MAC_FLURRY;
            case   2:
                return HAMBURGUESA;
                default:
                    return  null;

        }

    }
}
