package com.open_training.maccomponets.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.open_training.maccomponets.objects.ejercicio1.Cafe;
import com.open_training.maccomponets.objects.ejercicio1.Hamburguesa;
import com.open_training.maccomponets.objects.ejercicio1.Macflurry;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by jonathan.zepeda on 10/10/2017.
 */
public class NuevaOrden implements Parcelable {
    private List<Hamburguesa> hamburguesaList;
    private List<Macflurry> macflurryList;
    private List<Cafe> cafeList;

    public NuevaOrden() {
    }

    public NuevaOrden(List<Hamburguesa> hamburguesaList, List<Macflurry> macflurryList, List<Cafe> cafeList) {
        this.hamburguesaList = hamburguesaList;
        this.macflurryList = macflurryList;
        this.cafeList = cafeList;
    }

    //METODOS SET Y GET

    public List<Hamburguesa> getHamburguesaList() {
        return hamburguesaList;
    }

    public void setHamburguesaList(List<Hamburguesa> hamburguesaList) {
        this.hamburguesaList = hamburguesaList;
    }

    public List<Macflurry> getMacflurryList() {
        return macflurryList;
    }

    public void setMacflurryList(List<Macflurry> macflurryList) {
        this.macflurryList = macflurryList;
    }

    public List<Cafe> getCafeList() {
        return cafeList;
    }

    public void setCafeList(List<Cafe> cafeList) {
        this.cafeList = cafeList;
    }

    public static Creator<NuevaOrden> getCREATOR() {
        return CREATOR;
    }

    //FIN DE METODOS SET Y GET

    protected NuevaOrden(Parcel in) {
        if (in.readByte() == 0x01) {
            hamburguesaList = new ArrayList<Hamburguesa>();
            in.readList(hamburguesaList, Hamburguesa.class.getClassLoader());
        } else {
            hamburguesaList = null;
        }
        if (in.readByte() == 0x01) {
            macflurryList = new ArrayList<Macflurry>();
            in.readList(macflurryList, Macflurry.class.getClassLoader());
        } else {
            macflurryList = null;
        }
        if (in.readByte() == 0x01) {
            cafeList = new ArrayList<Cafe>();
            in.readList(cafeList, Cafe.class.getClassLoader());
        } else {
            cafeList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (hamburguesaList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(hamburguesaList);
        }
        if (macflurryList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(macflurryList);
        }
        if (cafeList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(cafeList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<NuevaOrden> CREATOR = new Parcelable.Creator<NuevaOrden>() {
        @Override
        public NuevaOrden createFromParcel(Parcel in) {
            return new NuevaOrden(in);
        }

        @Override
        public NuevaOrden[] newArray(int size) {
            return new NuevaOrden[size];
        }
    };
}