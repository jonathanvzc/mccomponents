package com.open_training.maccomponets.objects.holders;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.open_training.maccomponets.R;
import com.open_training.maccomponets.objects.ejercicio2.MacOrden;

import javax.crypto.Mac;

/**
 * Created by jonathan.zepeda on 02/11/2017.
 */

public class ItemOrderHolder extends RecyclerView.ViewHolder {

    private CardView cvContainer;
    private ImageView ivProduct;
    private TextView tvCon;
    private TextView tvSin;

    public ItemOrderHolder(View itemView) {
        super(itemView);

        cvContainer = itemView.findViewById(R.id.cv_container);
        ivProduct = itemView.findViewById(R.id.iv_product);
        tvCon = itemView.findViewById(R.id.tv_con);
        tvSin = itemView.findViewById(R.id.tv_sin);
    }

    public CardView getCvContainer() {
        return cvContainer;
    }

    public void setCvContainer(CardView cvContainer) {
        this.cvContainer = cvContainer;
    }

    public ImageView getIvProduct() {
        return ivProduct;
    }

    public TextView getTvCon() {
        return tvCon;
    }


    public TextView getTvSin() {
        return tvSin;
    }

    public void setMacHolder(MacOrden macOrden){
        ivProduct.setImageResource(macOrden.getFoodType().getImage());
        String strCon ="<b>Con </b>";
        String strSin ="<b>Sin </b>";


        for (String temp: macOrden.getConList()){
            strCon +=temp +",";
        }
        tvCon.setText(Html.fromHtml(strCon));

        for (String temp: macOrden.getSinList()){
            strSin +=temp +",";
        }
        tvSin.setText(Html.fromHtml(strSin));


    }


}
