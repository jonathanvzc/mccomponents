package com.open_training.maccomponets.objects.ejercicio2;

/**
 * Created by jonathan.zepeda on 23/11/2017.
 */

public class MacOption {
    private String strOption;
    private  boolean selected;



    public MacOption() {
    }

    public MacOption(String strOption, boolean selected) {
        this.strOption = strOption;
        this.selected = selected;
    }

    public void setStrOption(String strOption) {
        this.strOption = strOption;
    }


    public String getStrOption() {

        return strOption;
    }


    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {

        this.selected = selected;
    }
}
