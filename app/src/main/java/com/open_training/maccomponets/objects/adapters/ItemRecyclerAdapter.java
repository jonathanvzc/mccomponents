package com.open_training.maccomponets.objects.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.open_training.maccomponets.objects.ejercicio2.MacOrden;
import com.open_training.maccomponets.objects.holders.ItemOrderHolder;

import java.util.List;

/**
 * Created by jonathan.zepeda on 14/11/2017.
 */

public class ItemRecyclerAdapter extends RecyclerView.Adapter<ItemOrderHolder> {

    private List<MacOrden> macOrdenList;
    private Activity activity;
    private int resourceLayout;

    public ItemRecyclerAdapter(List<MacOrden> macOrdenList, Activity activity, int resourceLayout) {
        this.macOrdenList = macOrdenList;
        this.activity = activity;
        this.resourceLayout = resourceLayout;
    }

    @Override
    public ItemOrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemOrderHolder(LayoutInflater.from(activity).inflate(resourceLayout,parent,false));
    }


    @Override
    public void onBindViewHolder(ItemOrderHolder holder, int position) {
        //renderiza el layout
        MacOrden mcOrden=macOrdenList.get(position);

        holder.setMacHolder(mcOrden);
        holder.getCvContainer().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

            }
        });
    }

    @Override
    public int getItemCount() {
        return macOrdenList.size();
    }
}