package com.open_training.maccomponets.objects.ejercicio1;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jonathan.zepeda on 17/10/2017.
 */

public class Macflurry implements Parcelable {
    private boolean topin;
    private boolean oreo;
    private boolean mym;

    protected Macflurry(Parcel in) {
        topin = in.readByte() != 0x00;
        oreo = in.readByte() != 0x00;
        mym = in.readByte() != 0x00;
    }


    public Macflurry() {
    }

    public Macflurry(boolean topin, boolean oreo, boolean mym) {
        this.topin = topin;
        this.oreo = oreo;
        this.mym = mym;
    }


    //SET Y GET

    public boolean isTopin() {
        return topin;
    }

    public void setTopin(boolean topin) {
        this.topin = topin;
    }

    public boolean isOreo() {
        return oreo;
    }

    public void setOreo(boolean oreo) {
        this.oreo = oreo;
    }

    public boolean isMym() {
        return mym;
    }

    public void setMym(boolean mym) {
        this.mym = mym;
    }

    public static Creator<Macflurry> getCREATOR() {
        return CREATOR;
    }

    //FIN SET Y GET



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (topin ? 0x01 : 0x00));
        dest.writeByte((byte) (oreo ? 0x01 : 0x00));
        dest.writeByte((byte) (mym ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Macflurry> CREATOR = new Parcelable.Creator<Macflurry>() {
        @Override
        public Macflurry createFromParcel(Parcel in) {
            return new Macflurry(in);
        }

        @Override
        public Macflurry[] newArray(int size) {
            return new Macflurry[size];
        }
    };
}